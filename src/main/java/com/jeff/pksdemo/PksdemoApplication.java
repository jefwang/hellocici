package com.jeff.pksdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication
/*
public class PksdemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(PksdemoApplication.class, args);
	}

}
*/

public class PksdemoApplication extends SpringBootServletInitializer {
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(PksdemoApplication .class);
	}

	public static void main(String[] args) {
		SpringApplication.run(PksdemoApplication.class, args);
	}
}
