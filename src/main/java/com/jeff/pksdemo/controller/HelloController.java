package com.jeff.pksdemo.controller;


import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/hello")
public class HelloController {

    @RequestMapping(value="/", method= RequestMethod.GET)
    public ResponseEntity<?> hello(){

        return ResponseEntity.ok().body("Hello World v2");
    }

}
